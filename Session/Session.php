<?php
namespace Tarantool\Session;

use Tarantool\Client\Client;
use Tarantool\Client\Connection\StreamConnection;
use Tarantool\Client\Packer\PeclPacker;
use Tarantool\Client\Packer\PurePacker;
use Tarantool\Client\Schema\Space;

/**
 * Created by PhpStorm.
 * User: Dm
 * Date: 07.04.2018
 * Time: 10:04
 */

class Session extends \yii\web\Session
{
    /**
     *
     */
    const STORAGE_DISC = 'vinyl';

    /**
     *
     */
    const STORAGE_MEMORY = 'memtx';

    /**
     *
     */
    const PACKER_PURE = PurePacker::class;

    /**
     *
     */
    const PACKER_PECL = PeclPacker::class;

    /**
     *
     */
    public const SCHEMA = [
        'id' => 'string',
        'expire' => 'unsigned',
        'data' => 'string'
    ];

    /**
     * @var string
     */
    public $server = "tcp://tarantool:3301"; //unix:///tmp/tarantool_instance.sock

    /**
     * @var float
     */
    public $socket_timeout = 5.0;

    /**
     * @var float
     */
    public $connect_timeout = 5.0;

    /**
     * @var bool
     */
    public $tcp_nodelay = true;

    /**
     * @var string
     */
    public $sessionSpace = 'session';

    /**
     * @var string
     */
    public $sessionStorage = self::STORAGE_DISC;

    /**
     * @var string
     */
    public $username = 'tarantool';

    /**
     * @var string
     */
    public $password = 'password';

    /**
     * @var string
     */
    public $packer = self::PACKER_PURE;

    /**
     * @var
     */
    private $space;

    /**
     * @var
     */
    private $client;

    /**
     * @var
     */
    private $connection;

    /**
     * @return bool
     */
    public function getUseCustomStorage(): bool
    {
        return true;
    }

    /**
     * @param bool $delete
     * @return bool
     * @throws \Exception
     */
    public function regenerateID($delete = false) :? bool
    {
        $id = session_id();
        if(empty($id)){
            return false;
        }
        if ($this->getIsActive()) {
            session_regenerate_id($delete);
        }
        $newID = session_id();
        $check = false;
        try {
            $check = $this->getSpace()->select([$id])->getData();
        } catch (\RuntimeException $e) {
        }
        $expire = time() + $this->getTimeout();
        if ($check) {
            if ($delete) {
                try {
                    $this->getSpace()->delete([$id]);
                    $this->getSpace()->insert([$newID, $expire, $check[0][2]]);
                } catch (\RuntimeException $e) {
                    print_r($e->getMessage());
                }
            } else {
                try {
                    $this->getSpace()->insert([$newID, $expire, $check[0][2]]);
                } catch (\RuntimeException $e) {
                    print_r($e->getMessage());
                }
            }
        } else {
            try {
                $this->getSpace()->insert([$newID, $expire, '']);
            } catch (\RuntimeException $e) {
                print_r($e->getMessage());
            }
        }
        return true;
    }

    /**
     * @return StreamConnection
     */
    protected function getDbConnection(): StreamConnection
    {
        if($this->connection === null) {
            return $this->connection = new StreamConnection(
                $this->server,
                [
                    'socket_timeout' => $this->socket_timeout,
                    'connect_timeout' => $this->connect_timeout,
                    'tcp_nodelay' => $this->tcp_nodelay
                ]
            );
        }
        if ($this->connection instanceof StreamConnection) {
            return $this->connection;
        }
        throw new \RuntimeException('Set Session connection instance of StreamConnection');
    }

    /**
     * @return string
     */
    public function getRealSessionSpace(): string
    {
        return $this->sessionSpace.'_'.$this->sessionStorage;
    }

    /**
     * @return Client
     * @throws \Exception
     */
    public function getClient(): Client
    {
        if($this->client === null) {
            $client = new Client($this->getDbConnection(), new $this->packer);
            $client->authenticate($this->username, $this->password);
            return $this->client = $client;
        }
        if ($this->client instanceof Client) {
            return $this->client;
        }

        throw new \RuntimeException('Set Session client');
    }

    /**
     * @return Space
     * @throws \Exception
     */
    public function getSpace(): Space
    {
        if($this->space === null) {
            return  $this->space = $this->getClient()->getSpace($this->getRealSessionSpace());
        }
        if ($this->space instanceof Space) {
            return $this->space;
        }
        throw new \RuntimeException('Check Session space');
    }

    /**
     * @throws \Exception
     */
    public function init()
    {
        $table = new \Tarantool\Mapper\Mapper($this->getClient());
        if (!$table->getSchema()->hasSpace($this->getRealSessionSpace())) {
            try {
                $space = $table->getSchema()->createSpace($this->getRealSessionSpace(), [
                    'engine' => $this->sessionStorage,
                    'properties' => self::SCHEMA
                ]);
                $space->createIndex([
                    'name' => 'id',
                    'type' => 'tree',
                    'fields' => ['id'],
                    'unique' => true
                ]);
                $this->space = $this->getSpace();
            } catch (\RuntimeException $e) {
            }
        }
        parent::init();
    }

    /**
     * @param string $id
     * @return array|string
     * @throws \Exception
     */
    public function readSession($id)
    {
        $data = false;
        try {
            $session = $this->getSpace()->select([$id])->getData();
            if ($session) {
                $data = $session[0][2];
            }
        } catch (\RuntimeException $e) {
            print_r($e->getMessage());
        }
        return (string)$data;
    }

    /**
     * @param string $id
     * @param string $data
     * @return bool|null|\Tarantool\Client\Response
     * @throws \Exception
     */
    public function writeSession($id, $data)
    {
        $expire = time() + $this->getTimeout();
        $result = null;
        $check = false;
        try {
            $check = $this->getSpace()->select([$id])->getData();
        } catch (\RuntimeException $e) {
            print_r($e->getMessage());
        }
        if ($check) {
            try {
                $result = $this->getSpace()->upsert([$id], [['=', 1, $expire], ['=', 2, $data]]);
            } catch (\RuntimeException $e) {
                print_r($e->getMessage());
            }
        } else {
            try {
                $result = $this->getSpace()->insert([$id, $expire, $data]);
            } catch (\RuntimeException $e) {
                print_r($e->getMessage(), '');
            }
        }
        return $result;
    }

    /**
     * @param string $id
     * @return bool
     * @throws \Exception
     */
    public function destroySession($id): bool
    {
        try {
            $this->getSpace()->delete([$id]);
        } catch (\RuntimeException $e) {
            print_r($e->getMessage());
        }
        return true;
    }

    /**
     * Ends the current session and store session data.
     */
    public function close(): bool
    {
        try {
            if ($this->getIsActive()) {
                YII_DEBUG ? session_write_close() : @session_write_close();
            }
        } catch (\Exception $e) {
        }
        return true;
    }

    /**
     * @param $maxLifetime
     * @return bool
     */
    public function gcSession($maxLifetime): bool
    {
        // TODO
        return true;
    }
}
