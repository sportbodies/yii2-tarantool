 **Yii 2 session in Tarantool db**  

     ...
     components => [  
         ...  
         'session' => [
             // 'class' => CHttpSession::class,
             'class' => Tarantool\Session\Session::class,
             'sessionSpace'=>'Session',
             'server' => "tcp://sid_tarantool:3301",
             'socket_timeout' => 5.0,
             'connect_timeout' => 5.0,
             'tcp_nodelay' => true,
             'sessionStorage' => Tarantool\Session\Session::STORAGE_DISC, // or STORAGE_MEMORY
             'packer' => Tarantool\Session\Session::PACKER_PURE, // or PACKER_PECL
             'username' => 'tarantool',
             'password' => 'password'
         ],
         ...  
     ]  
     ...  
 